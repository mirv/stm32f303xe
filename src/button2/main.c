#include <stm32f303xe.h>
#include <stm32f3xx.h>

#define LED2_PIN         (5)
#define BUTTON_USER_PIN (13)

/**
 * The main system clock (HPE) is by default set to 1/8 supply clock.
 * For the NUCLEO-F303RE board in question (MB1136 C-03), the input
 * clock is set to MCO, fixed at 8MHz. So this means that the system
 * ticker clock going to run at 1MHz by default.
 * STK_CTRL &= ~(CLKSOURCE)
 * Note: it seems the default setup code for this board doesn't do that,
 * and leaves sys_tick at MCO frequency (8MHz). So a custom setup is
 * given to properly set the clock source.
 *
 * LSE for this board is configured to use a 32.768kHz crystal by
 * default.
 */

#define MICROSECOND (1)
#define MILLISECOND (1000*MICROSECOND)
#define SECOND      (1000*MILLISECOND)

unsigned int led_on = 0;

void SysTick_Handler(void)
{
    return;
}

void EXTI15_10_IRQHandler(void)
{
	EXTI->PR |= 1 << BUTTON_USER_PIN;

	if (led_on) {
		GPIOA->BRR = GPIO_BSRR_BS_5;
		led_on = 0;
	} else {
		GPIOA->BSRR = GPIO_BSRR_BS_5;
		led_on = 1;
	}

	return;
}

static void setup_systick(uint32_t tick_value)
{
	SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk);    /* Disable systick while we change the settings. */
	SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk); /* Set systick to run from AHB/8 */
	SysTick->CTRL |= (SysTick_CTRL_TICKINT_Msk);    /* Counting down to zero asserts systick exception request. */

    SysTick->VAL = 0;                               /* Clear previous values. Any write clears this to zero,
                                                     * no matter the actual write value itself.
                                                     */

    SysTick->LOAD = (tick_value - 1);               /* 1 <= STK_LOAD <= 0x00FFFFFF */

    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;       /* Enable systick counter. */
}

int main(void)
{
	/**
	 * LED2 is on PA.5 (Port A, Pin 5). This is a peripheral that needs to be
	 * clocked. If the clock is not enabled on that peripheral, then the
	 * register values may not be readable.
	 */
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	SET_BIT(GPIOA->MODER, 1 << (LED2_PIN << 1)); /* Configure PIN5 as output */
	//GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_5); /* Optional, ensure PIN 5 configured as push/pull */
	SET_BIT(GPIOA->OSPEEDR, GPIO_OSPEEDER_OSPEEDR5); /* Set speed */

	/**
	 * BUTTON_USER is on PortC, Pin 13. So enable peripheral clock to
	 * PortC as well.
	 */
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	CLEAR_BIT(GPIOC->MODER, 1 << (BUTTON_USER_PIN << 1)); /* Configure for input. */
	/* GPIOC->MODER &= ~(GPIO_MODER_MODER13_0); */  /* Configure for input. */
	/* SET_BIT(GPIOC->OSPEEDR, GPIO_OSPEEDER_OSPEEDR13); */
	SET_BIT(GPIOC->PUPDR, 1 << (BUTTON_USER_PIN << 1)); /* Configure for pullup */
	/* SET_BIT(GPIOC->PUPDR, 2 << (BUTTON_USER_PIN << 1)); */ /* Configure for pulldown */

	/* Need to enable access to SYSCFG peripheral for STM32, before poking at interrupt settings. */
	SET_BIT(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN);
	__NOP(); /* Let the peripheral be clocked to ensure we can access it. */

	/* Setup SYSCFG so that PortC, Pin 13, is mapped to the proper external interrupt control line. */
	uint32_t syscfg_exti_index = (BUTTON_USER_PIN >> 2);
	uint32_t syscfg_exti_mask = 0x000F << (4 * (BUTTON_USER_PIN & 0x03));
	uint32_t syscfg_exti_value = 0x0002 << (4 * (BUTTON_USER_PIN & 0x03)); /* 0x02 corresponds to GPIOC. */
	SYSCFG->EXTICR[syscfg_exti_index] &= ~(syscfg_exti_mask);
	SYSCFG->EXTICR[syscfg_exti_index] |= syscfg_exti_value;

	EXTI->IMR |= (1 << BUTTON_USER_PIN); /* Do not mask interrupt. */
	EXTI->EMR |= (1 << BUTTON_USER_PIN); /* Do not mask event. */

	/**
	 * Both rising and falling edges could be used, but the button the board suffers
	 * rather badly from bouncing. So chances are that a "push" would actually trigger
	 * both a rising and falling edge much of the time.
	 * To account for that, a small delay could be started from within the interrupt
	 * to check the button status, but that's too much for this example.
	 */
	//EXTI->RTSR |= (1 << BUTTON_USER_PIN); /* Rising-edge trigger selection register. */
	EXTI->FTSR |= (1 << BUTTON_USER_PIN); /* Falling-edge trigger selection register. */

	/* Set Button IRQ to lowest priority. */
	NVIC_SetPriority(EXTI15_10_IRQn, NVIC_EncodePriority(0, 2, 0));
	/* Button IRQ is hooked up to external interrupt lines [15:10]. */
	NVIC_EnableIRQ(EXTI15_10_IRQn);

	setup_systick(1 * SECOND);

	while(1) {
		__WFI();
	}
}
