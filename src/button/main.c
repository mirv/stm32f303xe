#include <stm32f303xe.h>
#include <stm32f3xx.h>

#define LED2_PIN         (5)
#define BUTTON_USER_PIN (13)
/**
 * The main system clock (HPE) is by default set to 1/8 supply clock.
 * For the NUCLEO-F303RE board in question (MB1136 C-03), the input
 * clock is set to MCO, fixed at 8MHz. So this means that the system
 * ticker clock going to run at 1MHz by default.
 * STK_CTRL &= ~(CLKSOURCE)
 * Note: it seems the default setup code for this board doesn't do that,
 * and leaves sys_tick at MCO frequency (8MHz). So a custom setup is
 * given to properly ste the clock source.
 *
 * LSE for this board is configured to use a 32.768kHz crystal by
 * default.
 */

#define MICROSECOND (1)
#define MILLISECOND (1000*MICROSECOND)
#define SECOND      (1000*MILLISECOND)

void SysTick_Handler(void)
{
    return;
}

static void setup_systick(uint32_t tick_value)
{
	SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk);    /* Disable systick while we change the settings. */
	SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk); /* Set systick to run from AHB/8 */
	SysTick->CTRL |= (SysTick_CTRL_TICKINT_Msk);    /* Counting down to zero asserts systick exception request. */

    SysTick->VAL = 0;                               /* Clear previous values. Any write clears this to zero,
                                                     * no matter the actual write value itself.
                                                     */

    SysTick->LOAD = (tick_value - 1);               /* 1 <= STK_LOAD <= 0x00FFFFFF */

    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;       /* Enable systick counter. */
}

int main(void)
{
	/**
	 * LED2 is on PA.5 (Port A, Pin 5). This is a peripheral that needs to be
	 * clocked. If the clock is not enabled on that peripheral, then the
	 * register values may not be readable.
	 */
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	/* Configure PIN5 as output */
	/* All lines have the same effect, choose one */
	SET_BIT(GPIOA->MODER, (1 << (LED2_PIN << 1)));
	//GPIOA->MODER |= GPIO_MODER_MODER5_0;
	//GPIOA->MODER |= (1 << (5 << 1));

	/* Optional, ensure PIN 5 configured as push/pull */
	//GPIOA->OTYPER &= ~(GPIO_OTYPER_OT_5);

	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5; /* Set speed */
	GPIOA->BSRR = GPIO_BSRR_BS_5; // using macro from stm32f303xe.h


	/**
	 * BUTTON_USER is on PortC, Pin 13. So enable peripheral clock to
	 * PortC as well.
	 */
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	CLEAR_BIT(GPIOC->MODER, 1 << (BUTTON_USER_PIN << 1)); /* Configure for input. */
	/* GPIOC->MODER &= ~(GPIO_MODER_MODER13_0); */  /* Configure for input. */
	/* SET_BIT(GPIOC->OSPEEDR, GPIO_OSPEEDER_OSPEEDR13); */
	SET_BIT(GPIOC->PUPDR, 1 << (BUTTON_USER_PIN << 1)); /* Configure for pullup */
	/* SET_BIT(GPIOC->PUPDR, 2 << (BUTTON_USER_PIN << 1)); */ /* Configure for pulldown */


	setup_systick(1 * SECOND);

	while(1) {

		if (!(GPIOC->IDR & (1 << BUTTON_USER_PIN))) {
			/* Turn LED ON */
			/* All lines have same effect, choose one */
			GPIOA->BSRR = GPIO_BSRR_BS_5; // using macro from stm32f303xe.h
			//GPIOA->BSRR |= (1 << LED_2_PIN);
			//GPIOA->ODR = 0xFFFF; // not recommended
		}

		__WFI();

		/* Turn LED OFF */
		/* All lines have same effect, choose one */
		//GPIOA->BSRR = GPIO_BSRR_BR_5;
		GPIOA->BRR = GPIO_BSRR_BS_5;
		//GPIOA->BSRR = (1 << (LED_2_PIN + 16));
		//GPIOA->BRR |= (1 << LED_2_PIN);
		//GPIOA->ODR = 0x0000; // not recommended

		__WFI();
	}
}
