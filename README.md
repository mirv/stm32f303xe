STM32F303xE Examples
====================

## Introduction
The examples contained in this project are intended to be used for learning
bare metal programming on Cortex-M4 devices. CMSIS, or CMSIS compatible,
definitions are used to provide essential register definitions, but wrapper
libraries are avoided where possible.

## Examples

### Blinky
Blinky is a simple example that sets up SystTick and peripheral clock, and
periodically turns the on-board LED on and off. Basically the "Hello, World!"
example.

### Button
Button is a slight extension over the blinky example that pauses any blinking
of LED2 while the user button is pressed.

### Button2
Button2 is a modification on Button, where interrupts are used to detect a
button push, rather than polling. This introduces how to enable external
interrupts on the STM32F303RE board. Note that the button is subject to
aggressive bouncing, so only falling edge triggered interrupts are used.

### Blinky2
Blilnk2 is modification on Blinky, where instead of a periodic on/off of the
LED, a morse code sequence is displayed instead. This shows how to use simple
logic to sequence output on a timered basis. Note that the STM32 timers are
not suitable for continued count with compare value interrupts and so could
lose timing precision; instead use SysTick to continually run a base unit of
100ms and adjust a state machine on each tick.

## License
Unless explicitly mentioned otherwise, all code falls under a GPLv3 license.
STM32 header and intialisation routines may be incompatible, but I'm not a
lawyer, so the stipulation is that all of _my_ code falls under GPLv3, and
where binary distribution is perfomed, then all of _my_ code must be made
available. If there is doubt, the solution is simple: please do not binary
distribute, but instead release full source code with build instructions.

Feel free to use what you have _learned_ here in a commercial environment.
Chances are you would have to reimplement everything anyway because such an
environment would likely contain custom ASICs.

Please note that the SConstruct and SConscript files are covered by a BSD
2-clause license. These files build the otherwise GPL covered code, but are
not a part of it directly, and this allows the build scripts to be taken and
used in other projects however people want.
