#include <stm32f303xe.h>

#define LED_2_PIN 5

#define LED_ON() (GPIOA->BSRR = GPIO_BSRR_BS_5)
#define LED_OFF() (GPIOA->BRR = GPIO_BSRR_BS_5)

/**
 * The main system clock (HPE) is by default set to 1/8 supply clock.
 * For the NUCLEO-F303RE board in question (MB1136 C-03), the input
 * clock is set to MCO, fixed at 8MHz. So this means that the system
 * ticker clock is going to run at 1MHz by default.
 * STK_CTRL &= ~(CLKSOURCE)
 * Note: it seems the default setup code for this board doesn't do that,
 * and leaves sys_tick at MCO frequency (8MHz). So a custom setup is
 * given to properly set the clock source.
 *
 * LSE for this board is configured to use a 32.768kHz crystal by
 * default.
 */

#define MICROSECOND (1)
#define MILLISECOND (1000*MICROSECOND)
#define SECOND      (1000*MILLISECOND)

/**
 * Rules for International Morse Code:
 * 1. The length of a dot is one unit.
 * 2. The length of a dash is three units.
 * 3. The space between parts of the same letter is one unit.
 * 4. The space between letters is three units.
 * 5. The space between words is seven units.
 *
 * To comply with point 5, the easiest approach is to make a space
 * simply a seven "null" units (i.e neither dot nor dash). But, a
 * space is essentially a letter between words, and therefore needs
 * three units before and after. If each letter automatically includes
 * three units after, then a space becomes only four "null" units,
 * followed by the standard three units after.
 *
 * The maximum length of a single character is '0', which is 5 dashes.
 *
 * Morse code has twenty-six letters and ten numerals, plus an extra
 * for the "space" character described above. Each code has up to five
 * dots/dashes, meaning it can be encoded into 8bits:
 *     [7 - 5] : length of code (0 - 5)
 *     [4 - 0] : 0 for dot, 1 for dash.
 * ' ' (space) is a special case and can be checked for with an index
 * value into the encoded array.
 */

#define CODE(x, y) (((x) << 5) | (y))

/**
 * Binary representation is LSB first, but Latin base languages write
 * numerically as MSB, so just bear that in mind when looking at the
 * encodings.
 * Below uses 0b to write binary, which is non-standard C and so will
 * probably need GCC to compile.
 */
uint8_t m_morse_code[] = {
	CODE(2, 0b10),    // A
	CODE(4, 0b0001),  // B
	CODE(4, 0b0101),  // C
	CODE(3, 0b001),   // D
	CODE(1, 0b0),     // E
	CODE(4, 0b0100),  // F
	CODE(3, 0b011),   // G
	CODE(4, 0b0000),  // H
	CODE(2, 0b00),    // I
	CODE(4, 0b1110),  // J
	CODE(3, 0b101),   // K
	CODE(4, 0b0010),  // L
	CODE(2, 0b11),    // M
	CODE(2, 0b01),    // N
	CODE(3, 0b111),   // O
	CODE(4, 0b0110),  // P
	CODE(4, 0b1011),  // Q
	CODE(3, 0b010),   // R
	CODE(3, 0b000),   // S
	CODE(1, 0b1),     // T
	CODE(3, 0b100),   // U
	CODE(4, 0b1000),  // V
	CODE(3, 0b110),   // W
	CODE(4, 0b1001),  // X
	CODE(4, 0b1101),  // Y
	CODE(4, 0b0011),  // Z
	CODE(5, 0b11110), // 1
	CODE(5, 0b11100), // 2
	CODE(5, 0b11000), // 3
	CODE(5, 0b10000), // 4
	CODE(5, 0b00000), // 5
	CODE(5, 0b00001), // 6
	CODE(5, 0b00011), // 7
	CODE(5, 0b00111), // 8
	CODE(5, 0b01111), // 9
	CODE(5, 0b11111), // 0
	CODE(7, 0b11111)  // Special case to indicate end of word.
};

typedef enum {
	WAIT = 0,
	DOT_DASH,
	GAP
} MorseState;

///< Current blinking state.
MorseState m_state = WAIT;

///< Counter; meaning changes with state, but generally units of state remaining.
uint8_t m_counter = 0;

///< Code for lettering state. Shifted
uint8_t m_code = 0;

///< Number of dot/dash elements remaining for current code.
uint8_t m_code_remaining = 0;

uint8_t m_sequence[] = {
	'S' - 'A',
	'O' - 'A',
	'S' - 'A',
	36 // word break
};
uint16_t m_sequence_index = 0;

static void sequence_next(void)
{
	m_sequence_index = (m_sequence_index + 1) & 0x0003;
	m_code = m_morse_code[m_sequence[m_sequence_index]];
	m_code_remaining = m_code >> 5;
}

void SysTick_Handler(void)
{
	switch (m_state) {
	case WAIT:
		// Setup a switch to the first character, and start blinking.
		m_sequence_index = 3; sequence_next();
		m_counter = 1;
		m_state = GAP;
		break;
	case DOT_DASH:
		if (--m_counter == 0) {
			LED_OFF();
			if (--m_code_remaining) {
				m_code >>= 1;

				// Break to next dot/dash.
				m_counter = 1;
			} else {
				// No code remaining on the current character.
				// Move to next character and check for letter or word break.
				sequence_next();

				if (m_code == 0xFF) {
					// Word break. Set the appropriate gap count and move to next character.
					m_counter = 7;
					sequence_next();
				} else {
					// Letter break.
					m_counter = 3;
				}
			}
			m_state = GAP;
		}
		break;
	case GAP:
		if (--m_counter == 0) {
			// Gap finished, move on to next dot/dash.
			if (m_code_remaining) {
				if (m_code & 0x01) {
					// Dash.
					m_counter = 3;
				} else {
					// Dot.
					m_counter = 1;
				}
				m_state = DOT_DASH;
				LED_ON();
			} else {
				// This is either the end of the sequence, wait for more.
				m_state = WAIT;
			}
		}
		break;
	}
    return;
}

static void setup_systick(uint32_t tick_value)
{
	SysTick->CTRL &= ~(SysTick_CTRL_ENABLE_Msk);    /* Disable systick while we change the settings. */
	SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk); /* Set systick to run from AHB/8 */
	SysTick->CTRL |= (SysTick_CTRL_TICKINT_Msk);    /* Counting down to zero asserts systick exception request. */

    SysTick->VAL = 0;                               /* Clear previous values. Any write clears this to zero,
                                                     * no matter the actual write value itself.
                                                     */

    SysTick->LOAD = (tick_value - 1);               /* 1 <= STK_LOAD <= 0x00FFFFFF */

    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;       /* Enable systick counter. */
}

int main(void)
{
	/**
	 * LED2 is on PA.5 (Port A, Pin 5). This is a peripheral that needs to be
	 * clocked. If the clock is not enabled on that peripheral, then the
	 * register values may not be readable.
	 */
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	/* Configure PIN5 as output */
	GPIOA->MODER |= GPIO_MODER_MODER5_0;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5;

	LED_OFF();

	setup_systick(100 * MILLISECOND);

	while(1) {
		__WFI();
	}
}
