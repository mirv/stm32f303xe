'''
Copyright 2022 mirv

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
'''

import os
from pathlib import Path

env = Environment(ENV = os.environ)

top_dir = Path(Dir('#').path)
build_dir = Path("build")

toolchain_path = "/opt/gcc-arm-none-eabi-8-2019-q3-update/bin"
toolchain_prefix = os.path.join(toolchain_path, "arm-none-eabi-")

device = "cortex-m4"

env["AR"] = toolchain_prefix + "ar"
env["AS"] = toolchain_prefix + "as"
env["CC"] = toolchain_prefix + "gcc"
env["CXX"] = toolchain_prefix + "gcc"
env["LINK"] = toolchain_prefix + "gcc"
env["RANLIB"] = toolchain_prefix + "ranlib"
env["OBJCOPY"] = toolchain_prefix + "objcopy"
env["PROGSUFFIX"] = ".elf"

env["CPPPATH"] = top_dir/"src"/"common"

# Going to override the default linker command here to inject a custom variable
# called WLIBS, which is a list of libraries to include in the --whole-archive
# linker group. SCons doesn't support this natively, so need manually it ourselves.
env["WLIBS"] = []
env["_WLIBFLAGS"] = "${_stripixes(LIBLINKPREFIX, WLIBS, LIBLINKSUFFIX, LIBPREFIXES, LIBSUFFIXES, __env__)}"
env["LINKCOM"] = "$LINK -o $TARGET $LINKFLAGS $__RPATH $SOURCES $_LIBDIRFLAGS $_LIBFLAGS -Wl,--whole-archive $_WLIBFLAGS -Wl,--no-whole-archive"

env.Append(CCFLAGS = [
    "-Os",
    "-Wall",
    "-mcpu="+device,
    "-mlittle-endian",
    "-mthumb",
    "-DSTM32F303xE",
])

env.Append(CPPDEFINES = [
    "STM32F303xE"
])

env.Append(LINKFLAGS = [
    "-mcpu="+device,
    "-mlittle-endian",
    "-mthumb",
    "-Wl,--gc-sections"
])
#    "-Wl,--whole-archive",

ld_file = top_dir/"src"/"common"/"stm32f303xe_flash.ld"
env.Append(LINKFLAGS = "-T"+str(ld_file))

# Common library components. This will append the necessary
# library dependency to the exported env LIBS. Perhaps not
# the best way to design such things, but there we are.
SConscript(os.path.join("src", "common", "SConscript"),
           variant_dir=build_dir/"common",
           duplicate=0,
           exports={"env" : env})

# Generate intel hex format file from an input elf.
hex_builder = Builder(action = "$OBJCOPY -Oihex $SOURCE $TARGET",
                      suffix = ".hex",
                      src_suffix = ".elf")
env.Append(BUILDERS = {"Hex" : hex_builder})

# MD5 hash. Mostly used on the hex files (as they would be
# distributed and in need of verification).
md5_builder = Builder(action = "md5sum $SOURCE > $TARGET",
                      suffix = ".md5",
                      src_suffix = ".hex")
env.Append(BUILDERS = {"Md5" : md5_builder})

# Generate an openocd configuration file so that
# the flash command knows the hex file to flash.

openocd_cfg_template = """
#
# This file generated automatically. Do not edit.
#
source [find interface/stlink-v2-1.cfg]

source [find target/stm32f3x.cfg]

# use hardware reset, connect under reset
reset_config srst_only srst_nogate

init
reset init
halt
flash write_image erase %s
reset run
shutdown

"""

def build_cfg(target, source, env) :
    """
    Generate an openocd flashing configuration file.
    """
    generated = openocd_cfg_template % (source[0].path)
    fd = open(target[0].path, 'w')
    fd.write(generated)
    fd.close()
    return 0

cfg_builder = Builder(action = build_cfg, suffix = ".cfg", src_suffix=".hex")
env.Append(BUILDERS = {"FlashConfig" : cfg_builder})

projects = [
    "blinky",
    "button",
    "button2",
    "blinky2"
]

for project in projects :
    SConscript(os.path.join("src", project, "SConscript"),
               variant_dir=build_dir/project,
               duplicate=0,
               exports={"env" : env})

# Delete everything. Removes the build folder entirely.
env.Command("nuke", None, Delete("build"))
